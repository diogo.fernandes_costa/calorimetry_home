from functions import m_json
from functions import m_pck
#Für den jeweiligen Versuch kommentiere ich die Zeilen Code, die nicht notwendig sind. Sonst wird der Raspberry Pi in meinem Falle überlastet
path1 = "/home/pi/calorimetry_home/datasheets/setup_newton.json"

path2 = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"

#Kontrolle, ob die Sensoren angeschlossen sind
m_pck.check_sensors()

#Ausgeben der Metadaten aus dem Versuchsaufbau
metadata1 = m_json.get_metadata_from_setup(path1)
#metadata2 = m_json.get_metadata_from_setup(path2)

#Ausgeben der Sensor-Serienummern
m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata1)
#m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata2)

#Archivieren der json-Dateien in ein bestimmtes Archiv
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path1, '/home/pi/calorimetry_home/archiv/Newton_Experiment')
#m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path2, '/home/pi/calorimetry_home/archiv/Heat_capacity_Experiment')

#Ausgabe der Temperatur-Zeit-Daten aus dem jeweilgen Versuch
data1 = m_pck.get_meas_data_calorimetry(metadata1)
#data2 = m_pck.get_meas_data_calorimetry(metadata2)

#Speichern der h5-Datei
m_pck.logging_calorimetry(data1, metadata1, '/home/pi/calorimetry_home/archiv/Newton_Experiment', '/home/pi/calorimetry_home/datasheets')
#m_pck.logging_calorimetry(data2, metadata2, '/home/pi/calorimetry_home/archiv/Heat_capacity_Experiment', '/home/pi/calorimetry_home/datasheets')

print(":)")
